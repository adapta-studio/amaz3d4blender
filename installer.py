import bpy
from pathlib import Path
from subprocess import run
import sys

print("Starting amaz3d for Blender installation")

PYTHON_PATH = sys.executable

from importlib import import_module, invalidate_caches

def ensure_pip() -> None:
    print("Installing pip... "),

    completed_process = run([PYTHON_PATH, "-m", "ensurepip"])

    if completed_process.returncode == 0:
        print("Successfully installed pip")
    else:
        raise Exception("Failed to install pip.")

def modules_path() -> Path:
    modules_path = Path(bpy.utils.script_path_user(), "addons", "modules")
    modules_path.mkdir(exist_ok=True, parents=True)

    # set user modules path at beginning of paths for earlier hit
    if sys.path[1] != modules_path:
        sys.path.insert(1, str(modules_path))

    return modules_path


def install_module(module_name, package_name=None, global_name=None) -> None:
    if package_name is None:
        package_name = module_name

    if global_name is None:
        global_name = module_name

    path = modules_path()

    completed_process = run(
        [
            PYTHON_PATH,
            "-m",
            "pip",
            "install",
            package_name,
            "-t",
            str(path),
        ],
        capture_output=True,
        text=True,
    )

    if completed_process.returncode != 0:
        print("Please try manually installing amaz3d")
        raise Exception(
            """
            Failed to install amaz3d.
            See console for manual install instruction.
            """
        )

def is_pip_available() -> bool:
    try:
        import_module("pip")  # noqa F401
        return True
    except ImportError:
        return False

def install_dependencies() -> None:
    if not is_pip_available():
        ensure_pip()

    try:
        import amaz3dpy
    except ModuleNotFoundError:
        install_module("amaz3dpy")

def _import_dependencies() -> None:
    import_module("amaz3dpy")

def ensure_dependencies() -> None:

    try:
        install_dependencies()
        invalidate_caches()
        _import_dependencies()
        print("Found all dependencies, proceed with loading")
    except ImportError:
        raise Exception(
            "Cannot automatically ensure amaz3d dependencies. Please restart Blender!"
        )

if __name__ == "__main__":
    ensure_dependencies()