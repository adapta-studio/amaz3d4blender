from bpy.props import StringProperty, BoolProperty, CollectionProperty, IntProperty, EnumProperty, PointerProperty, FloatProperty
from bpy.utils import register_class, unregister_class
from bpy.types import Panel, UIList, PropertyGroup, AddonPreferences, Operator
import bpy
from datetime import datetime, timezone
import math
import os
import tempfile

from .installer import ensure_dependencies
ensure_dependencies()

import amaz3dpy
from dateutil import parser
import timeago

bl_info = {
    "name": "Amaz3D4Blender",
    "description": "Blender AddOn for AMAZ3D",
    "author": "Adapta Studio",
    "version": (0, 1),
    "blender": (3, 0, 0),
    "location": "View3D > Sidebar",
    "category": "Import-Export",
    "warning": "This plugin will install amaz3dpy"
}



class CustomNamedTemporaryFile:
    """
    This custom implementation is needed because of the following limitation of tempfile.NamedTemporaryFile:

    > Whether the name can be used to open the file a second time, while the named temporary file is still open,
    > varies across platforms (it can be so used on Unix; it cannot on Windows NT or later).
    """

    def __init__(self, mode='wb', delete=True, suffix=".gltf"):
        self._mode = mode
        self._delete = delete
        self._suffix = suffix
        self.__enter__()

    def __enter__(self):
        # Generate a random temporary file name
        self.name = os.path.join(
            tempfile.gettempdir(),
            os.urandom(24).hex() +
            self._suffix)
        # Open the file in the given mode
        self._tempFile = open(self.name, self._mode)
        return self.name

    def __exit__(self, exc_type, exc_val, exc_tb):
        try:
            self.name.close()
        finally:
            if self._delete:
                os.remove(self.name.name)


class Amaz3DClientInstance():
    amaz3dclient: amaz3dpy.clients.Amaz3DClient = None


class Amaz3dForBlenderPreferences(AddonPreferences):
    bl_idname = __name__

    email: StringProperty(
        name="Email",
    )

    password: StringProperty(
        name="Password",
        subtype='PASSWORD'
    )

    url: StringProperty(
        name="Url",
        default="amaz3d_backend.adapta.studio"
    )

    use_ssl: BoolProperty(
        name="Use SSL",
        default=True
    )

    def draw(self, context):
        layout = self.layout
        layout.label(
            text="Set your username and password for accessing AMAZ3D and restart Blender for applying changes")
        layout.prop(self, "email")
        layout.prop(self, "password")
        layout.prop(self, "url")
        layout.prop(self, "use_ssl")


def convert_size(size_bytes):
    size_bytes = int(size_bytes)
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])


def populate_optimizations(project):
    project.optimizations.clear()

    for i in Amaz3DClientInstance.amaz3dclient.optimizations():
        if i.status == "completed":
            item = project.optimizations.add()
            item.name = i.name
            item.id = i.id
            item.format = i.outputFormat.replace("format_", "")
            item.triangleCount = str(i.objectModelResult.triangleCount)
            item.size = convert_size(i.objectModelResult.fileSizeBytes)
            item.status = i.status


def populate_optimization_templates(context):
    context.scene.amaz3d_optimization_templates.clear()
    scn = context.scene

    for i in Amaz3DClientInstance.amaz3dclient.optimization_templates():
        if len(i.optimizationTemplateItems) > 0:
            item = scn.amaz3d_optimization_templates.add()
            item.name = i.name
            item.id = i.id


def populate_projects(context):
    context.scene.amaz3d_projects.clear()
    scn = context.scene
    now = datetime.utcnow()
    now = now.replace(tzinfo=timezone.utc)

    for i in Amaz3DClientInstance.amaz3dclient.projects():
        item = scn.amaz3d_projects.add()
        item.name = i.name
        item.id = i.id
        item.conversionStatus = i.conversionStatus
        item.lastActivityAt = timeago.format(
            parser.parse(
                i.lastActivityAt),
            now) if i.lastActivityAt is not None else ""
        item.triangleCount = str(
            i.objectModel.triangleCount) if i.objectModel.triangleCount is not None else ""
        item.size = convert_size(
            i.objectModel.fileSizeBytes) if i.objectModel.fileSizeBytes is not None else ""
        item.optimizations.clear()


class AMAZ3D_OT_loadProjects(Operator):
    bl_idname = "amaz3d.load_projects"
    bl_label = "Load projects"
    bl_description = "Load projects"
    bl_options = {'REGISTER'}

    def execute(self, context):
        # clean
        context.scene.amaz3d_projects.clear()
        Amaz3DClientInstance.amaz3dclient.clear_projects()
        # load
        Amaz3DClientInstance.amaz3dclient.load_projects()
        populate_projects(context)
        return {'FINISHED'}


class AMAZ3D_OT_loadOptimizations(Operator):
    bl_idname = "amaz3d.load_optimizations"
    bl_label = "Load optimizations"
    bl_description = "Load optimizations"
    bl_options = {'REGISTER'}

    def execute(self, context):
        project = context.scene.amaz3d_projects[context.scene.amaz3d_project_index]
        Amaz3DClientInstance.amaz3dclient.select_a_project(project.id)
        # clean
        Amaz3DClientInstance.amaz3dclient.clear_optimizations()
        project.optimizations.clear()
        # load
        Amaz3DClientInstance.amaz3dclient.load_optimizations()
        populate_optimizations(project)
        return {'FINISHED'}


class AMAZ3D_OT_loadOptimizationTemplates(Operator):
    bl_idname = "amaz3d.load_optimization_templates"
    bl_label = "Load optimization templates"
    bl_description = "Load optimization templates"
    bl_options = {'REGISTER'}

    def execute(self, context):
        Amaz3DClientInstance.amaz3dclient.load_optimization_templates()
        populate_optimization_templates(context)
        return {'FINISHED'}


class AMAZ3D_OT_clearProjects(Operator):
    bl_idname = "amaz3d.clear_projects"
    bl_label = "Clear projects"
    bl_description = "Clear projects"
    bl_options = {'REGISTER'}

    def execute(self, context):
        context.scene.amaz3d_project_index
        context.scene.amaz3d_projects.clear()
        Amaz3DClientInstance.amaz3dclient.clear_projects()
        return {'FINISHED'}


class AMAZ3D_OT_clearOptimizations(Operator):
    bl_idname = "amaz3d.clear_optimizations"
    bl_label = "Clear optimizations"
    bl_description = "Clear optimizations"
    bl_options = {'REGISTER'}

    def execute(self, context):
        project = context.scene.amaz3d_projects[context.scene.amaz3d_project_index]
        Amaz3DClientInstance.amaz3dclient.select_a_project(project.id)
        Amaz3DClientInstance.amaz3dclient.clear_optimizations()
        project.optimizations.clear()
        return {'FINISHED'}


class AMAZ3D_OT_createProject(Operator):
    bl_idname = "amaz3d.create_project"
    bl_label = "Create a project from the selected object"

    def execute(self, context):
        tf = CustomNamedTemporaryFile(mode='w', suffix='.glb')
        bpy.ops.export_scene.gltf(
            filepath=tf.name,
            export_format='GLB',
            use_selection=True)
        Amaz3DClientInstance.amaz3dclient.create_project(
            bpy.context.selected_objects[0].name, tf.name)
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)


class AMAZ3D_OT_deleteProject(Operator):
    bl_idname = "amaz3d.delete_project"
    bl_label = "Delete the selected project"

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

    def execute(self, context):
        scn = bpy.context.scene
        project = scn.amaz3d_projects[context.scene.amaz3d_project_index]
        Amaz3DClientInstance.amaz3dclient.select_a_project(project.id)
        Amaz3DClientInstance.amaz3dclient.delete_selected_project()
        scn.amaz3d_projects.remove(context.scene.amaz3d_project_index)
        return {'FINISHED'}


class AMAZ3D_OT_createOptimization(Operator):
    bl_idname = "amaz3d.create_optimization"
    bl_label = "Create an optimization for the selected project"

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

    def execute(self, context):
        scn = bpy.context.scene
        project = scn.amaz3d_projects[context.scene.amaz3d_project_index]
        Amaz3DClientInstance.amaz3dclient.select_a_project(project.id)
        opt = scn.amaz3d_new_optimization
        format = amaz3dpy.models.OptimizationOutputFormat[opt.output_format]
        name = opt.name

        if name == "":
            level_feature_importance = ""
            if opt.feature_importance == 0:
                level_feature_importance = "Low"
            if opt.feature_importance == 1:
                level_feature_importance = "Medium"
            if opt.feature_importance == 2:
                level_feature_importance = "High"
            name = "Opt " + str(int(opt.face_reduction * 100)) + \
                "% " + level_feature_importance

        create_optimization_params = {
            'name': name,
            'format': format
        }

        params = amaz3dpy.models.OptimizationParams()
        params.face_reduction = opt.face_reduction
        params.feature_importance = opt.feature_importance
        params.uv_seam_importance = opt.uv_seam_importance
        params.preserve_boundary_edges = opt.preserve_boundary_edges
        params.preserve_hard_edges = opt.preserve_hard_edges
        params.preserve_smooth_edges = opt.preserve_smooth_edges
        params.retexture = opt.retexture
        params.merge_duplicated_uv = opt.merge_duplicated_uv
        params.remove_isolated_vertices = opt.remove_isolated_vertices
        params.remove_non_manifold_faces = opt.remove_non_manifold_faces
        params.remove_duplicated_faces = opt.remove_duplicated_faces
        params.remove_duplicated_boundary_vertices = opt.remove_duplicated_boundary_vertices
        params.remove_degenerate_faces = opt.remove_degenerate_faces
        params.project_normals = opt.project_normals
        params.use_vertex_mask = opt.use_vertex_mask
        params.resize_images = opt.resize_images
        params.normals_weighting = opt.normals_weighting
        params.contrast = opt.contrast
        params.joined_simplification = opt.joined_simplification
        params.normals_scaling = opt.normals_scaling
        params.minimum_face_number = opt.minimum_face_number
        params.remove_meshes_by_size = opt.remove_meshes_by_size
        params.remove_meshes_by_count = opt.remove_meshes_by_count

        create_optimization_params['params'] = params

        Amaz3DClientInstance.amaz3dclient.create_optimization(
            **create_optimization_params)
        return {'FINISHED'}


class AMAZ3D_OT_createOptimizationFromOptimizationTemplates(Operator):
    bl_idname = "amaz3d.create_optimization_from_option_template"
    bl_label = "Create an optimization using a preset"

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

    def execute(self, context):
        scn = bpy.context.scene
        project = scn.amaz3d_projects[scn.amaz3d_project_index]
        preset = scn.amaz3d_optimization_templates[context.scene.amaz3d_optimization_templates_index]
        Amaz3DClientInstance.amaz3dclient.select_a_project(project.id)
        Amaz3DClientInstance.amaz3dclient.select_an_optimization_template(
            preset.id)
        name = context.scene.amaz3d_new_optimization_from_template.name
        Amaz3DClientInstance.amaz3dclient.create_optimizations_from_preset(
            name)
        return {'FINISHED'}


class AMAZ3D_OT_downloadOptimization(Operator):
    bl_idname = "amaz3d.download_optimization"
    bl_label = "Download an optimization"

    def execute(self, context):

        project = context.scene.amaz3d_projects[context.scene.amaz3d_project_index]
        opt = project.optimizations[project.optimization_index]
        Amaz3DClientInstance.amaz3dclient.select_a_project(project.id)
        Amaz3DClientInstance.amaz3dclient.select_an_optimization(opt.id)
        optimization = Amaz3DClientInstance.amaz3dclient.get_selected_optimization()

        if len(optimization.objectModelResult.additionals) > 0:
            self.report({'ERROR'}, "Unable to download not embedded projects.")
            return {'CANCELLED'}

        if optimization.objectModelResult:
            tf = tempfile.NamedTemporaryFile(delete=False)
            Amaz3DClientInstance.amaz3dclient.download_selected_optimization(
                dst_file_path=tf.name, results_files=True, additionals=False)
            file_name = optimization.objectModelResult.name

            if file_name.endswith(".obj"):
                bpy.ops.import_scene.obj(filepath=tf.name)

            if file_name.endswith(".fbx"):
                bpy.ops.import_scene.fbx(
                    filepath=tf.name,
                    ignore_leaf_bones=True,
                    force_connect_children=True,
                    automatic_bone_orientation=True)

            if file_name.endswith(".stl"):
                bpy.ops.import_mesh.stl(filepath=tf.name)

            if file_name.endswith(".glb") or file_name.endswith(".gltf"):
                bpy.ops.import_scene.gltf(filepath=tf.name)

            if file_name.endswith(".3ds"):
                return {'CANCELLED'}

            bpy.context.selected_objects[0].name = optimization.name
            bpy.context.selected_objects[0].data.name = optimization.name
            tf.close()
            return {'FINISHED'}

        return {'CANCELLED'}


class AMAZ3D_OT_defaultOptimizationParams(Operator):
    bl_idname = "amaz3d.default_optimization_params"
    bl_label = "Default optimization params"

    def execute(self, context):
        scn = bpy.context.scene
        scn.amaz3d_new_optimization.name = ""
        scn.amaz3d_new_optimization.output_format = "format_glb"
        scn.amaz3d_new_optimization.face_reduction = 0.5
        scn.amaz3d_new_optimization.feature_importance = 0
        scn.amaz3d_new_optimization.uv_seam_importance = 0
        scn.amaz3d_new_optimization.preserve_boundary_edges = 0
        scn.amaz3d_new_optimization.preserve_hard_edges = True
        scn.amaz3d_new_optimization.preserve_smooth_edges = True
        scn.amaz3d_new_optimization.retexture = True
        scn.amaz3d_new_optimization.merge_duplicated_uv = False
        scn.amaz3d_new_optimization.remove_isolated_vertices = True
        scn.amaz3d_new_optimization.remove_non_manifold_faces = True
        scn.amaz3d_new_optimization.remove_duplicated_faces = True
        scn.amaz3d_new_optimization.remove_duplicated_boundary_vertices = True
        scn.amaz3d_new_optimization.remove_degenerate_faces = True
        scn.amaz3d_new_optimization.project_normals = False
        scn.amaz3d_new_optimization.use_vertex_mask = False
        scn.amaz3d_new_optimization.resize_images = 0
        scn.amaz3d_new_optimization.normals_weighting = 0
        scn.amaz3d_new_optimization.contrast = 0.5
        scn.amaz3d_new_optimization.joined_simplification = True
        scn.amaz3d_new_optimization.normals_scaling = 0
        scn.amaz3d_new_optimization.minimum_face_number = 0
        scn.amaz3d_new_optimization.remove_meshes_by_size = 0
        scn.amaz3d_new_optimization.remove_meshes_by_count = 0
        return {'FINISHED'}


class AMAZ3D_PG_optimizationsCollection(PropertyGroup):
    id: StringProperty()
    format: StringProperty()
    triangleCount: StringProperty()
    size: StringProperty()
    status: StringProperty()


class AMAZ3D_PG_optimizationTemplatesCollection(PropertyGroup):
    id: StringProperty()
    name: StringProperty()
    outputFormat: StringProperty()


class AMAZ3D_PG_projectsCollection(PropertyGroup):
    id: StringProperty()
    optimizations: CollectionProperty(
        name="optimizationsCollection",
        type=AMAZ3D_PG_optimizationsCollection)
    optimization_index: IntProperty()
    optimization_templates: CollectionProperty(
        name="optimizationTemplatesCollection",
        type=AMAZ3D_PG_optimizationTemplatesCollection)
    optimization_templates_index: IntProperty()
    conversionStatus: StringProperty()
    lastActivityAt: StringProperty()
    triangleCount: StringProperty()
    size: StringProperty()
    optimizationsCount: IntProperty()
    allowsNormalsBaking: BoolProperty()
    allowsVertexMaskPolygonReduction: BoolProperty()


class AMAZ3D_PG_newOptimization(PropertyGroup):
    id: StringProperty()

    face_reduction: FloatProperty(
        name="Face reduction",
        description="A float that contains the percentage of reduction in terms of triangles",
        default=0.5,
        soft_max=0.99,
        soft_min=0.0,
        max=0.99,
        min=0.0)

    feature_importance: IntProperty(
        name="Feature importance",
        description="An integer that contains how much we want to preserve the features of the mesh",
        default=0,
        soft_max=2,
        soft_min=0,
        max=2,
        min=0)

    uv_seam_importance: IntProperty(
        name="Uv Seam Importance",
        description="An integer that preserves the UV seam curves that we have in our model",
        default=0,
        soft_max=2,
        soft_min=0,
        max=2,
        min=0)

    preserve_boundary_edges: IntProperty(
        name="Preserve boundary edges",
        description="An integer that decides how to treat the edges on the border of the mesh",
        default=0,
        min=0,
        max=2)

    preserve_hard_edges: BoolProperty(
        name="Preserve hard edges",
        description="A boolean that decides how to treat hard edges on the border of the mesh",
        default=True)

    preserve_smooth_edges: BoolProperty(
        name="Preserve smooth edges",
        description="A boolean true that decides if the smooth edges are parsed in the software or not",
        default=True)

    retexture: BoolProperty(
        name="Retexture",
        description="A boolean that decides if the UV coordinates must be recalculated on the textures of a simplified object",
        default=True)

    merge_duplicated_uv: BoolProperty(
        name="Merge duplicated UV",
        description="A boolean that contains UV coordinates overlapped in the same planes",
        default=False)

    remove_isolated_vertices: BoolProperty(
        name="Remove isolated vertices",
        description="A boolean that removes isolated vertices from the other triangles",
        default=True)

    remove_non_manifold_faces: BoolProperty(
        name="Remove non manifold faces",
        description="A boolean that removes manifold triangles",
        default=True)

    remove_duplicated_faces: BoolProperty(
        name="Remove duplicated faces",
        description="A boolean that removes duplicates faces",
        default=True)

    remove_duplicated_boundary_vertices: BoolProperty(
        name="Remove duplicated boundary vertices",
        description="A boolean comes to remove UV duplicates",
        default=True)

    remove_degenerate_faces: BoolProperty(
        name="Remove degenerate faces",
        description="A boolean that removes degenerate faces",
        default=True)

    project_normals: BoolProperty(
        name="Use project normals",
        description="A boolean that uses the project normals",
        default=False)

    use_vertex_mask: BoolProperty(
        name="Use vertex mask",
        description="A boolean that decides if you want to use the vertex mask of the model",
        default=False)

    resize_images: IntProperty(
        name="Resize images",
        description="A number that defines the new resolution all textures will be resized to",
        default=0,
        max=8192,
        min=0)

    normals_weighting: FloatProperty(
        name="Resize images",
        description="A number that defines the strategy for calculating the weighted average of face normals used in the calculation of each vertex normal",
        default=0,
        max=1,
        min=0)

    contrast: FloatProperty(
        name="Contrast",
        description="A number that modulates the weights applied to face normals",
        default=0.5,
        max=1,
        min=0)

    joined_simplification: BoolProperty(
        name="Uniform simplification",
        description="A boolean that decides if a mesh will be simplified as a singular mesh",
        default=True)

    normals_scaling: FloatProperty(
        name="Use normal for optimization",
        description="An integer that check if the normals are going to be preserved during simplification",
        default=0,
        max=1,
        min=0)

    minimum_face_number: IntProperty(
        name="Minimum Face Number",
        description="An integer that defines the minumum polygon number for a mesh to be optimized",
        default=0,
        max=50,
        min=0)

    remove_meshes_by_size: FloatProperty(
        name="Remove Meshes By Size",
        description="A float that removes meshes with number of faces lower than the value chosen",
        default=0,
        max=0.1,
        min=0)

    remove_meshes_by_count: IntProperty(
        name="Remove Meshes By Count",
        description="An integer that removes meshes with number of faces lower than the value chosen",
        default=0,
        max=2000,
        min=0)

    output_format: EnumProperty(
        default="format_orig",
        items=[
            ('format_orig', 'orig', ''),
            ('format_obj', 'obj', ''),
            ('format_gltf', 'gltf', ''),
            ('format_glb', 'glb', ''),
            ('format_fbx', 'fbx', ''),
            ('format_stl', 'stl', '')
        ]
    )


class AMAZ3D_PG_newOptimizationFromTemplate(PropertyGroup):
    id: StringProperty()
    name: StringProperty()


class AMAZ3D_UL_optimizations(UIList):

    def draw_item(
            self,
            context,
            layout,
            data,
            item,
            icon,
            active_data,
            active_propname,
            index):
        split = layout.split(factor=0.3)

        try:
            id = " (" + item.id.split('-')[-1] + ")"
        except BaseException:
            id = ''

        split.label(text=item.name + id)
        split.label(text=item.status + " - " + item.format)
        split.label(text=item.triangleCount + " triangles - " + item.size)


class AMAZ3D_UL_projects(UIList):

    def draw_item(
            self,
            context,
            layout,
            data,
            item,
            icon,
            active_data,
            active_propname,
            index):
        split = layout.split(factor=0.3)

        try:
            id = " (" + item.id.split('-')[-1] + ")"
        except BaseException:
            id = ''

        split.label(text=item.name + id)
        split.label(text=item.conversionStatus + " - " + item.lastActivityAt)
        split.label(text=item.triangleCount + " triangles - " + item.size)


class AMAZ3D_UL_optimization_templates(UIList):

    def draw_item(
            self,
            context,
            layout,
            data,
            item,
            icon,
            active_data,
            active_propname,
            index):
        split = layout.split(factor=0.3)

        try:
            id = " (" + item.id.split('-')[-1] + ")"
        except BaseException:
            id = ''

        split.label(text=item.name + id)


class AMAZ3D_PT_projects_panel(Panel):
    bl_idname = 'AMAZ3D_PT_projects_panel'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Projects"
    bl_category = "AMAZ3D"

    def draw(self, context):
        layout = self.layout
        scn = bpy.context.scene
        row = layout.row()
        row.template_list(
            "AMAZ3D_UL_projects",
            "",
            scn,
            "amaz3d_projects",
            scn,
            "amaz3d_project_index",
            rows=2)
        col = row.column(align=True)
        col.operator("amaz3d.clear_projects", icon='CANCEL', text="")
        col.operator("amaz3d.load_projects", icon='TRIA_DOWN', text="")

        if len(context.scene.amaz3d_projects) > 0:
            col.separator()
            col.operator("amaz3d.delete_project", icon='TRASH', text="")

        active_object = context.active_object
        if active_object is not None and active_object.type == 'MESH' and (
                context.mode == 'EDIT_MESH' or active_object.select_get()):
            layout.separator()
            box = layout.box()
            row = box.row()
            row.label(text="Create a project")
            row = box.row()
            row.operator("amaz3d.create_project")


class AMAZ3D_PT_optimizations_panel(Panel):
    bl_idname = 'AMAZ3D_PT_optimizations_panel'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Optimizations"
    bl_category = "AMAZ3D"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scn = bpy.context.scene

        if len(context.scene.amaz3d_projects) > 0:
            row = layout.row()
            project = context.scene.amaz3d_projects[context.scene.amaz3d_project_index]
            row.template_list(
                "AMAZ3D_UL_optimizations",
                "",
                project,
                "optimizations",
                project,
                "optimization_index",
                rows=2)
            col = row.column(align=True)
            col.operator("amaz3d.clear_optimizations", icon='CANCEL', text="")
            col.operator(
                "amaz3d.load_optimizations",
                icon='TRIA_DOWN',
                text="")
            col.separator()
            col.operator(
                "amaz3d.download_optimization",
                icon='IMPORT',
                text="")

        box = layout.box()
        row = box.row()
        credits = Amaz3DClientInstance.amaz3dclient.get_wallet().value
        tmp = ""
        if credits == -9999:
            tmp = "unlimited"
        else:
            tmp = str(credits)
        row.label(text="Credits available: " + tmp)
        row = box.row()
        row.prop(scn.amaz3d_new_optimization, "name")
        row.operator(
            "amaz3d.default_optimization_params",
            icon='OPTIONS',
            text="")

        if len(context.scene.amaz3d_projects) > 0:
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "face_reduction")
            row.prop(scn.amaz3d_new_optimization, "feature_importance")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "uv_seam_importance")
            row.prop(scn.amaz3d_new_optimization, "preserve_boundary_edges")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "preserve_hard_edges")
            row.prop(scn.amaz3d_new_optimization, "preserve_smooth_edges")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "retexture")
            row.prop(scn.amaz3d_new_optimization, "merge_duplicated_uv")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "remove_isolated_vertices")
            row.prop(scn.amaz3d_new_optimization, "remove_non_manifold_faces")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "remove_duplicated_faces")
            row.prop(
                scn.amaz3d_new_optimization,
                "remove_duplicated_boundary_vertices")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "remove_degenerate_faces")
            row.prop(scn.amaz3d_new_optimization, "project_normals")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "use_vertex_mask")
            row.prop(scn.amaz3d_new_optimization, "resize_images")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "normals_weighting")
            row.prop(scn.amaz3d_new_optimization, "contrast")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "joined_simplification")
            row.prop(scn.amaz3d_new_optimization, "normals_scaling")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "minimum_face_number")
            row.prop(scn.amaz3d_new_optimization, "remove_meshes_by_size")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "remove_meshes_by_count")
            row = box.row()
            row.prop(scn.amaz3d_new_optimization, "output_format")
            row = box.row()
            row.operator("amaz3d.create_optimization")


class AMAZ3D_PT_optimization_templates_panel(Panel):
    bl_idname = 'AMAZ3D_PT_optimization_templates_panel'
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Presets"
    bl_category = "AMAZ3D"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scn = bpy.context.scene

        row = layout.row()
        row.template_list(
            "AMAZ3D_UL_optimization_templates",
            "",
            scn,
            "amaz3d_optimization_templates",
            scn,
            "amaz3d_optimization_templates_index",
            rows=2)
        col = row.column(align=True)
        col.operator(
            "amaz3d.load_optimization_templates",
            icon='TRIA_DOWN',
            text="")
        if len(context.scene.amaz3d_optimization_templates) > 0:
            row = layout.row()
            row.prop(scn.amaz3d_new_optimization_from_template, "name")
            row = layout.row()
            row.operator("amaz3d.create_optimization_from_option_template")


classes = (
    AMAZ3D_UL_optimizations,
    AMAZ3D_UL_optimization_templates,
    AMAZ3D_UL_projects,
    AMAZ3D_PT_projects_panel,
    AMAZ3D_PT_optimizations_panel,
    AMAZ3D_PT_optimization_templates_panel,
    AMAZ3D_OT_loadProjects,
    AMAZ3D_OT_clearProjects,
    AMAZ3D_OT_loadOptimizations,
    AMAZ3D_OT_clearOptimizations,
    AMAZ3D_OT_loadOptimizationTemplates,
    AMAZ3D_OT_createProject,
    AMAZ3D_OT_createOptimization,
    AMAZ3D_OT_createOptimizationFromOptimizationTemplates,
    AMAZ3D_OT_downloadOptimization,
    AMAZ3D_OT_deleteProject,
    AMAZ3D_OT_defaultOptimizationParams
)


def initialization():
    preferences = bpy.context.preferences
    addon_prefs = preferences.addons[__name__].preferences

    url = addon_prefs.url
    use_ssl = addon_prefs.use_ssl
    email = addon_prefs.email
    password = addon_prefs.password

    Amaz3DClientInstance.amaz3dclient = amaz3dpy.clients.Amaz3DClient(
        url=url, use_ssl=use_ssl, disable_auto_update=True)
    Amaz3DClientInstance.amaz3dclient.login(
        email, password, keep_the_user_logged_in=False)
    Amaz3DClientInstance.amaz3dclient.load_projects()

    bpy.types.Scene.amaz3d_projects = CollectionProperty(
        type=AMAZ3D_PG_projectsCollection)
    bpy.types.Scene.amaz3d_project_index = IntProperty()
    bpy.types.Scene.amaz3d_optimization_templates = CollectionProperty(
        type=AMAZ3D_PG_optimizationTemplatesCollection)
    bpy.types.Scene.amaz3d_optimization_templates_index = IntProperty()
    bpy.types.Scene.amaz3d_new_optimization = PointerProperty(
        type=AMAZ3D_PG_newOptimization)
    bpy.types.Scene.amaz3d_new_optimization_from_template = PointerProperty(
        type=AMAZ3D_PG_newOptimizationFromTemplate)

    for cls in classes:
        register_class(cls)


def register():
    register_class(Amaz3dForBlenderPreferences)
    register_class(AMAZ3D_PG_optimizationsCollection)
    register_class(AMAZ3D_PG_optimizationTemplatesCollection)
    register_class(AMAZ3D_PG_projectsCollection)
    register_class(AMAZ3D_PG_newOptimization)
    register_class(AMAZ3D_PG_newOptimizationFromTemplate)

    initialization()


def unregister():
    unregister_class(Amaz3dForBlenderPreferences)
    unregister_class(AMAZ3D_PG_optimizationsCollection)
    unregister_class(AMAZ3D_PG_optimizationTemplatesCollection)
    unregister_class(AMAZ3D_PG_projectsCollection)
    unregister_class(AMAZ3D_PG_newOptimization)
    unregister_class(AMAZ3D_PG_newOptimizationFromTemplate)

    for cls in reversed(classes):
        unregister_class(cls)

    Amaz3DClientInstance.amaz3dclient.close()
    Amaz3DClientInstance.amaz3dclient = None

    del bpy.types.Scene.amaz3d_projects
    del bpy.types.Scene.amaz3d_project_index
    del bpy.types.Scene.amaz3d_new_optimization
    del bpy.types.Scene.amaz3d_optimization_templates
    del bpy.types.Scene.amaz3d_optimization_templates_index
    del bpy.types.Scene.amaz3d_new_optimization_from_template


if __name__ == "__main__":
    register()
    bpy.ops.amaz3d.load_projects()
