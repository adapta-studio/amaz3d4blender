# amaz3d4blender

Amaz3D For Blender is an Add-On that brings most of Amaz3D functionalities within Blender.
This Add-on permits to create projects by selecting mesh objects directly from your Blender scene. It also permits to download optimizations into your scene directly without going through file downloading.
It is also possible to create new optimizations and to handle your projects.
In order to use this Add-on, an Amaz3D account is required.
For further information please visit: [Adapta Studio Website](https://adapta.studio)

## Installation and Authentication

Currently, amaz3d4blender supports only Blender 3.0.X and beyond.
To install this Add-on follow these steps:

1. Download the add-on on your computer (i.e. download this repo by clicking the download button by selecting the zip format)
2. Go to the Add-ons section in the Blender preferences
3. Click Install button 
4. Pick the add-on (select the zip file ) using the File Browser
5. Enable the add-on from the 'Add-ons' section level
6. Insert your Amaz3D credentials (in the add-on section).
7. Restart Blender (you need to do that everytime you change credentials)

## How to Use

If the Add-on installation is successful, you will be able to access Amaz3D functionalities from View3D > Sidebar
The listview permits to load your projects.
After selecting a project it is possible to load optimizations.
To create a new project just select an object from your viewport and press the "Create a project from the selected object" button.
To create an optimization, select a project first. Thus use the form to set optimization parameters.
To download a result on your scene select an optimization and use the download button on the right side of the optimizations list.